<?php
use \App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Caio Navas',
            'email'    => 'caio@caiomnavas.com.br',
            'password' => bcrypt('123456'),
        ]);
        User::create([
            'name'     => 'Caio Web Dev',
            'email'    => 'caio@caiowebdev.com.br',
            'password' => bcrypt('123456'),
        ]);
    }
}
