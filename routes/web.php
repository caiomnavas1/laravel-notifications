<?php

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/notifications', 'NotificationController@notifications');

Route::post('/comments', 'Posts\CommentController@store')->name('comment.store');

Route::resource('posts', 'Posts\PostController');

Route::put('/notifications-read', 'NotificationController@markAsRead');
Route::put('/notifications-all-read', 'NotificationController@markAllAsRead');

Broadcast::routes();
