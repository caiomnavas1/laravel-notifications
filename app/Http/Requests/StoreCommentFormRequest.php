<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id' => 'required|exists:posts,id',
            'title'   => 'required|min:3|max:100',
            'content'   => 'required|min:5|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Campo Título é obrigatório!',
            'title.min' => 'O campo Título deve ter pelo menos 3 caracteres.',
            'title.max' => 'O campo Título deve ter no máximo 100 caracteres.',
            'content.required' => 'O campo Comentário é obrigatório!',
            'title.min' => 'O campo Comentário deve ter pelo menos 5 caracteres.',
            'content.max' => 'O campo Comentário deve ter no máximo 10.000 caracteres.',
        ];
    }
}
