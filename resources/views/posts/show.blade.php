@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h2>{{ $post->title }}</h2>
                    </div>
                    <div class="col-6">
                        <a href="{{ route('posts.index') }}" class="btn btn-danger float-right">Voltar</a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                {{ $post->content }}
            </div>
        </div>
    </div>
    @include('posts.comments.comment')
@endsection
