<div class="col-md-12 mt-3">
    <div class="card">
        <div class="card-header text-center"><h4>Comentários</h4></div>
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(auth()->check())
                <form action="{{ route('comment.store') }}" method="post" class="form">
                    @csrf
                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                    <div class="form-group">
                        <input type="text" name="title" id="title" placeholder="Título" class="form-control">
                    </div>
                    <div class="form-group">
                        <textarea name="content" id="content" cols="30" rows="5" placeholder="Comentário"
                                  class="form-control"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">
                            Enviar comentário
                        </button>
                    </div>
                </form>
            @else
                <p class="text-center">
                    Você precisa estar logado(a) para comentar.<br>
                    <a href="{{ route('login') }}">Clique aqui para entrar</a>
                </p>
            @endif
            <hr>
            @forelse($post->comments as $comment)
                <blockquote class="blockquote">
                    <p class="mb-0">
                        <small>
                            <strong>{{ $comment->title }}</strong> em <strong>{{ $comment->created_at->format('d/m/Y') }}</strong>
                        </small>
                        <br>
                        {{ $comment->content }}
                    </p>
                    <footer class="blockquote-footer">Por {{ $comment->user->name }}</footer>
                </blockquote>
            @empty
                <p class="text-center">
                    Este post não possui comentários, seja o(a) primeiro(a)!
                </p>
            @endforelse
        </div>
    </div>
</div>
