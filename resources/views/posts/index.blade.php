@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Listagem dos Posts</h3>
            </div>

            <div class="card-body">
                @forelse($posts as $post)
                    <a href="{{ route('posts.show', $post->id) }}">
                        <h4>
                            {{ $post->title }} <span class="badge badge-secondary">{{ $post->comments->count() }}</span>
                        </h4>
                    </a>
                    <hr>
                @empty
                    <p>
                        Nenhum post cadastrado!
                    </p>
                @endforelse
            </div>
        </div>
    </div>
    <div class="row justify-content-center mt-3">
        {!! $posts->links() !!}
    </div>
@endsection
