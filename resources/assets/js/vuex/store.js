import Vue from 'vue'
import Vuex from 'vuex'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'

import notifications from "./modulos/notifications";

Vue.use(Vuex)
Vue.use(VueToast)

export default new Vuex.Store({
    modules: {
        notifications
    }
})
