export default {
    state: {
        items: []
    },
    mutations: {
        LOAD_NOTIFICATIONS(state, notifications){
            state.items = notifications
            if(state.items.length > 0) {
                document.title = `Laravel - ${state.items.length} Notificações`
            }else{
                document.title = `Laravel`
            }
        },
        MARK_AS_READ(state, id){
            let index = state.items.filter(notification => notification.id == id)
            state.items.splice(index, 1)
            if(state.items.length > 0) {
                document.title = `Laravel - ${state.items.length} Notificações`
            }else{
                document.title = `Laravel`
            }
        },
        MARK_ALL_AS_READ(state){
            state.items = []
            if(state.items.length > 0) {
                document.title = `Laravel - ${state.items.length} Notificações`
            }else{
                document.title = `Laravel`
            }
        },
        ADD_NOTIFICATION(state, notification){
            state.items.unshift(notification)
            if(state.items.length > 0) {
                document.title = `Laravel - ${state.items.length} Notificações`
            }else{
                document.title = `Laravel`
            }
        }
    },
    actions: {
        loadNotifications (context){
            axios.get('/notifications').then(
                response => {
                    context.commit('LOAD_NOTIFICATIONS', response.data.notifications)
                })
        },
        markAsRead(context, params){
            axios.put('/notifications-read', params)
                .then(() => context.commit('MARK_AS_READ', params.id))
        },
        markAllAsRead(context, params){
            axios.put('/notifications-all-read', params)
                .then(() => context.commit('MARK_ALL_AS_READ'))
        }
    }
}
